using System.Text;
using Microsoft.Azure.EventHubs;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using IoTHubTrigger = Microsoft.Azure.WebJobs.EventHubTriggerAttribute;

namespace Dinoco.AzureFunctions.IotHubFunctions
{
    public static class IoTHubToCosmosDB
    {
        [FunctionName("IoTHubToCosmosDB")]
        public static void Run([IoTHubTrigger("messages/events", Connection = "IotHubConnStr",ConsumerGroup = "CosmosDB")]EventData message,
                               [CosmosDB("Dinoco", "IoTHubMessages", ConnectionStringSetting = "CosmosDBConnStr")] out object outDocument, ILogger log)
        {

            var messageFromIoTDevice = Encoding.UTF8.GetString(message.Body.Array);
            outDocument = messageFromIoTDevice;
        }
    }
}