using System.Dynamic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Azure.EventHubs;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.SignalRService;
using Microsoft.Extensions.Logging;
using IoTHubTrigger = Microsoft.Azure.WebJobs.EventHubTriggerAttribute;

namespace Dinoco.AzureFunctions.IotHubFunctions
{
    public static class IoTHubToSignalR
    {
        [FunctionName("IoTHubToSignalR")]
        public static async Task Run([IoTHubTrigger("messages/events", Connection = "IotHubConnStr", ConsumerGroup = "SignalR")] EventData message,
                               [SignalR(HubName = "DeviceTelemetryHub")] IAsyncCollector<SignalRMessage> signalRMessages,
                               ILogger log)
        {
            var messageFromIoTDevice = Encoding.UTF8.GetString(message.Body.Array);

            //Enhancement - This would go away with device twins as then we could tag our device and inject it into our message
            string StoreId = GetPropertyValue(JsonSerializer.Deserialize<ExpandoObject>(messageFromIoTDevice), "StoreID");

            await signalRMessages.AddAsync(
                new SignalRMessage
                {
                    Target = "DeviceTelemetry",
                    GroupName = $"Store{StoreId}",
                    Arguments = new[] { messageFromIoTDevice }
                })
                .ConfigureAwait(false);

        }

        private static string GetPropertyValue(ExpandoObject exObj, string key)
        {
            var val = exObj.Where(p => p.Key == key).Select(p => p.Value).SingleOrDefault();
            return val.ToString();
        }
    }
}