﻿namespace Dinoco.Shared.Models.Telemetry
{
    public class FuelTankTelemetry : DeviceTelemetryBase
    {
        public const decimal MaxTankCapacity = 1000;

        public decimal E85 { get; set; }
        public decimal Allinol { get; set; }
        public decimal Diesel { get; set; }

        public FuelTankTelemetry()
            : base()
        {
            base.DeviceType = "FuelTank";
        }
    }
}
