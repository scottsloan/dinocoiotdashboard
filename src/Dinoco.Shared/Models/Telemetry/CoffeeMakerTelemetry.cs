﻿using Dinoco.Shared.Enums.Devices;

namespace Dinoco.Shared.Models.Telemetry
{
    public class CoffeeMakerTelemetry : DeviceTelemetryBase
    {
        public const double HeatLost = 0.2d;
        public const double MinimumTemp = 160.0d;
        public const double MaximumTemp = 170.0d;
        public const int MaximumAge = 45;

        public double Temperature { get; set; }
        public CoffeeMakerState MachineState { get; set; }
        public int MinutesSinceLastBrew { get; set; }

        public CoffeeMakerTelemetry()
            : base()
        {
            base.DeviceType = "CoffeeMaker";

        }

        public CoffeeMakerTelemetry(CoffeeMakerState state, double Temp, int SinceLastBrew)
            : this()
        {
            this.Temperature = Temp;
            this.MachineState = state;
            this.MinutesSinceLastBrew = SinceLastBrew;
        }
    }
}
