﻿using System;

namespace Dinoco.Shared.Models.Telemetry
{
    public class DeviceTelemetryBase
    {
        public int StoreID { get; set; }

        public string DeviceType { get;  set; }

        public DateTime Timestamp { get; set; }

        public DeviceTelemetryBase()
        {
            StoreID = 1;
            Timestamp = DateTime.Now;
        }
    }
}
