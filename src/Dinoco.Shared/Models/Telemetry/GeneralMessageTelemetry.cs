﻿namespace Dinoco.Shared.Models.Telemetry
{
    public class GeneralMessageTelemetry : DeviceTelemetryBase
    {
        public string Message { get; set; }

        public GeneralMessageTelemetry()
        {

        }

        public GeneralMessageTelemetry(string MessageType, string message)
                : base()
        {
            base.DeviceType = MessageType;
            this.Message = message;
        }
    }
}
