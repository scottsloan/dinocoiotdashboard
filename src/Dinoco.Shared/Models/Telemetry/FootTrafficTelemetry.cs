﻿namespace Dinoco.Shared.Models.Telemetry
{
    public class FootTrafficTelemetry : DeviceTelemetryBase
    {
        public int Souls { get; set; }

        public FootTrafficTelemetry(int souls)
            : base()
        {
            base.DeviceType = "FootTraffic";

            Souls = souls;
        }
    }
}
