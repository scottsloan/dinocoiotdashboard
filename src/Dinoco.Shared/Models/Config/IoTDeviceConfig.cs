﻿namespace Dinoco.Shared.Models.Config
{
    public class IoTDeviceConfig
    {
        public string DeviceID { get; set; }
        public string DeviceType { get; set; }
        public int StoreID { get; set; }
        public int OperationFreq { get; set; }

        public RegistrationInfo Registration { get; set; }

        public IoTDeviceConfig()
        {
        }
    }
}
