﻿namespace Dinoco.Shared.Models.Config
{
    public class RegistrationInfo
    {
        public string RegistrationType { get; set; }
        public string ConnectionString { get; set; }
    }
}
