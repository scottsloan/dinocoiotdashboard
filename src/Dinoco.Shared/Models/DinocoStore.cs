﻿namespace Dinoco.Shared.Models
{
    public class DinocoStore
    {
        public int StoreID { get; set; }

        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public string StorePhone { get; set; }
        public string StoreManager { get; set; }

        public decimal Lat { get; set; }
        public decimal Long { get; set; }
    }
}
