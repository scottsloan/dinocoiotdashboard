﻿namespace Dinoco.Shared.Enums.Devices
{
    public enum CoffeeMakerState
    {
        OFF = 0,
        BREWING = 1,
        KEEPWARM = 2,
        WARMING = 3
    }
}
