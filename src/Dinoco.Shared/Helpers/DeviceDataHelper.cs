﻿using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Dinoco.Shared.Helpers
{
    public static class DeviceDataHelper<T>
    {
        public static List<T> ParseMany(List<object> data)
        {
            List<T> results = new List<T>();

            foreach (var rec in data)
            {
                try
                {
                    var recAsType = ParseSingle(rec);

                    if (recAsType == null)
                        continue;

                    results.Add(recAsType);
                }catch(Exception)
                {

                }
            }

            return results;
        }

        public static T ParseSingle(object data)
        {
            if (data == null)
                return default(T);

            string json = data.ToString();

            if (String.IsNullOrEmpty(json))
                return default(T);

            try{
                return JsonSerializer.Deserialize<T>(json);
            }catch(Exception)
            {

            }

            return default(T);
        }
    }
}
