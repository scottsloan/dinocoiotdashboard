﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dinoco.DataAccess.Interfaces;
using Dinoco.Shared.Helpers;
using Dinoco.Shared.Models.Telemetry;
using Dinoco.WebDashboard.Models.Store;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dinoco.WebDashboard.Controllers
{
    public class StoreController : Controller
    {
        private readonly ILogger<StoreController> _logger;
        private readonly IStoreService _storeSrv;
        private readonly IDeviceService _devService;

        public StoreController(ILogger<StoreController> logger, IStoreService storeSrv, IDeviceService deviceSrv)
        {
            _logger = logger;
            _storeSrv = storeSrv;
            _devService = deviceSrv;
        }

        public async Task<IActionResult> Index(int id)
        {
            var store = await _storeSrv.GetStoreByStoreNumber(id);

            if (store == null)
                return new NotFoundResult();

            StoreIndexModel model = new StoreIndexModel(store);

            var _deviceSrv = await _devService.GetDevicesForStore(store.StoreID);

            foreach (string device in _deviceSrv)
            {
                List<object> d = await _devService.GetLastReadingsForDevice(store.StoreID, device, 1);

                switch (device)
                {
                    case "CoffeeMaker":
                        model.LastCoffeeMakerTelemetry = DeviceDataHelper<CoffeeMakerTelemetry>.ParseSingle(d.FirstOrDefault());
                        break;
                    case "FootTraffic":
                        model.LastFootTrafficTelemetry = DeviceDataHelper<FootTrafficTelemetry>.ParseSingle(d.FirstOrDefault());
                        break;
                    case "FuelTank":
                        model.LastFuelTankTelemetry = DeviceDataHelper<FuelTankTelemetry>.ParseSingle(d.FirstOrDefault());
                        break;
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DeviceHistory(DeviceHistoryQuery model)
        {
            List<object> sensorData = await _devService.GetLastReadingsForDevice(model.StoreID, model.DeviceType, 90);

            switch (model.DeviceType.ToLower())
            {
                case "coffeemaker":
                    var records = DeviceDataHelper<CoffeeMakerTelemetry>.ParseMany(sensorData);
                    return PartialView("_CoffeeMakerHistory", records);
                case "foottraffic":
                    var ftrecords = DeviceDataHelper<FootTrafficTelemetry>.ParseMany(sensorData);
                    return PartialView("_FootTrafficHistory", ftrecords);
                case "fueltank":
                    var furecords = DeviceDataHelper<FuelTankTelemetry>.ParseMany(sensorData);
                    return PartialView("_FuelTankHistory", furecords);
            }

            return new NotFoundResult();
        }
    }
}
