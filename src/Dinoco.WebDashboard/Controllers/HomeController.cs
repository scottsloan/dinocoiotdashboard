﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Dinoco.DataAccess.Interfaces;
using Dinoco.Shared.Helpers;
using Dinoco.Shared.Models.Telemetry;
using Dinoco.WebDashboard.Models;
using Dinoco.WebDashboard.Models.Home;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dinoco.WebDashboard.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IStoreService _storeSrv;
        private readonly IDeviceService _deviceSrv;

        public HomeController(ILogger<HomeController> logger, IStoreService storeSrv, IDeviceService deviceSrv)
        {
            _logger = logger;
            _storeSrv = storeSrv;
            _deviceSrv = deviceSrv;
        }

        public async Task<IActionResult> Index()
        {
            HomeIndexModel model = new HomeIndexModel();

            var _stores = await _storeSrv.GetAllStores();

            foreach (var store in _stores)
            {
                StoreOverview so = new StoreOverview(store);

                var _deviceTypes = await _deviceSrv.GetDevicesForStore(so.StoreID);

                foreach (string s in _deviceTypes)
                {
                    List<object> d = await _deviceSrv.GetLastReadingsForDevice(so.StoreID, s, 1);

                    switch (s)
                    {
                        case "CoffeeMaker":
                            CoffeeMakerTelemetry cmData = DeviceDataHelper<CoffeeMakerTelemetry>.ParseSingle(d.FirstOrDefault());
                            so.LoadCoffeeMakerResults(cmData);
                            break;
                        case "FootTraffic":
                            FootTrafficTelemetry ftData = DeviceDataHelper<FootTrafficTelemetry>.ParseSingle(d.FirstOrDefault());
                            so.LoadFootTrafficResults(ftData);
                            break;
                        case "FuelTank":
                            FuelTankTelemetry FutData = DeviceDataHelper<FuelTankTelemetry>.ParseSingle(d.FirstOrDefault());
                            so.LoadFuelTankResults(FutData);
                            break;
                    }
                }

                model.Stores.Add(so);
            }

            return View(model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<IActionResult> Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
