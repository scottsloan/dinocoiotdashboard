﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dinoco.DataAccess.Interfaces;
using Dinoco.Shared.Helpers;
using Dinoco.Shared.Models.Telemetry;
using Microsoft.AspNetCore.Mvc;

namespace Dinoco.WebDashboard.ViewComponents
{
    public class AlertsViewComponent : ViewComponent
    {
        private readonly IStoreService _storeSrv;
        private readonly IDeviceService _sensorSrv;

        public AlertsViewComponent(IStoreService storeSrv, IDeviceService sensorSrv)
        {
            _storeSrv = storeSrv;
            _sensorSrv = sensorSrv;
        }

        public async Task<IViewComponentResult> InvokeAsync(int StoreID = 0)
        {
            int _take = (StoreID == 0) ? 5 : 100;

            List<object> sensorData = await _sensorSrv.GetLastReadingsForDevice(StoreID, "Alert", _take);
            List<GeneralMessageTelemetry> records = DeviceDataHelper<GeneralMessageTelemetry>.ParseMany(sensorData);
            return View(records);
        }


    }
}
