﻿var ftChart = null;

function initializeTiles() {
    var ftCtx = document.getElementById('FuelTankChart');

    ftChart = new Chart(ftCtx, {
        type: 'bar',
        data: {
            labels: ['E85', 'Allinol', 'Diesel'],
            datasets: [{
                label: 'Tank Levels',
                data: [100, 100, 100],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    UpateFootTrafficTile({'Souls': 0});
}


function toFixed(num, fixed) {
    if (!num)
        num = 0;

    var re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?');
    return num.toString().match(re)[0];
}

function UpdateDeviceTile(payload) {

    if (typeof payload == "string") {
        payload = JSON.parse(payload);
    }

    if (payload.DeviceType == "FootTraffic") {
        UpateFootTrafficTile(payload);
    } else if (payload.DeviceType == "FuelTank") {
        UpateFuelTankTile(payload);
    } else if (payload.DeviceType == "CoffeeMaker") {
        UpateCoffemakerTile(payload);
    } else {

    }
}

function UpateFootTrafficTile(payload) {
    $(".telemetryValue").html(payload.Souls);
    $(".foottraffic_lastUpdate").html(getLastUpdateTimestamp());
}

function UpateCoffemakerTile(payload) {

    var stateAsString = "OFF";

    if (payload.MachineState == 1) {
        stateAsString = "Brewing";
    } else if (payload.MachineState == 2) {
        stateAsString = "Keeping Warm";
    } else if (payload.MachineState == 3) {
        stateAsString = "Warming";
    } else {
        stateAsString = "Broken";
    }

    $(".coffeeMaker_state").html(stateAsString);
    $(".coffeeMaker_temperature").html(toFixed(payload.Temperature,2) +"&deg;");
    $(".coffeeMaker_age").html(payload.MinutesSinceLastBrew);

    $(".coffeeMaker_lastUpdate").html(getLastUpdateTimestamp());
}

function getLastUpdateTimestamp() {
    return moment().format('L, LT');
}

function addChartJsData(chart, label, data) {
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });

    chart.update();
}

function clearChart(chart) {
    chart.data.label = [];

    chart.data.datasets.forEach((dataset) => {
        dataset.data = [];
    });

    chart.update();
}

function UpateFuelTankTile(payload) {
    clearChart(ftChart);
    addChartJsData(ftChart, 'E85', toFixed(payload.E85,2));
    addChartJsData(ftChart, 'Allinol', toFixed(payload.Allinol,2));
    addChartJsData(ftChart, 'Diesel', toFixed(payload.Diesel, 2));

    $(".fueltank_lastUpdate").html(getLastUpdateTimestamp());
}

function UpdateAlert(alertMessage) {

    if (!alertMessage || alertMessage == null)
        return;

    document.getElementsByClassName("Alerts_LastMessage")[0].innerHTML = alertMessage.Message;
    document.getElementsByClassName("Alerts_Timestamp")[0].innerHTML = alertMessage.Timestamp;
}