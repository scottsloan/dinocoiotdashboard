﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Dinoco.WebDashboard.Hubs
{
    public class DeviceTelemetryHub : Hub
    {
        public async Task JoinGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }

        public async Task LeaveGroup(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }
    }
}
