using Dinoco.DataAccess.CosmosDB.Services;
using Dinoco.DataAccess.InMemory.Services;
using Dinoco.DataAccess.Interfaces;
using Dinoco.WebDashboard.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Dinoco.WebDashboard
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddSingleton<IStoreService, CosmosDBStoreService>();
            //services.AddSingleton<IDeviceService, CosmosDBDeviceService>();
            services.AddSingleton<IStoreService, InMemoryStoreService>();
            services.AddSingleton<IDeviceService, InMemoryDeviceService>();
            services.AddControllersWithViews();

            services.AddSignalR()
                    .AddAzureSignalR(config =>
                    {
                        config.ConnectionString = Configuration.GetSection("AzureSignalR:ConnectionString").Value;
                    });

            services.AddApplicationInsightsTelemetry(Configuration["APPINSIGHTS_CONNECTIONSTRING"]);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                  name: "areas",
                  pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                );
            });

            app.UseAzureSignalR(routes =>
            {
                routes.MapHub<DeviceTelemetryHub>("/DeviceTelemetryHub");
            });
        }
    }
}
