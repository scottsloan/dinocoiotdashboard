﻿using Microsoft.AspNetCore.Mvc;

namespace Dinoco.WebDashboard.Areas.Manage.Controllers
{
    [Area("Manage")]
    public class ManageSiteController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
