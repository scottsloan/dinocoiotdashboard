﻿using Microsoft.AspNetCore.Mvc;

namespace Dinoco.WebDashboard.Areas.Admin.Controllers
{
    [Area("Manage")]
    public class ManageStoresController : Controller
    {
        public ManageStoresController()
        {

        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
