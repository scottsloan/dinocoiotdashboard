﻿namespace Dinoco.WebDashboard.Models.Store
{
    public class DeviceHistoryQuery
    {
        public int StoreID { get; set; }
        public string DeviceType { get; set; }
    }
}
