﻿using Dinoco.Shared.Models;
using Dinoco.Shared.Models.Telemetry;

namespace Dinoco.WebDashboard.Models.Store
{
    public class StoreIndexModel
    {
        public int StoreNumber { get; set; }

        public string AddressLine { get; set; }
        public string ManagerLine { get; set; }
        public string ContactLine { get; set; }

        public decimal Lat { get; set; }
        public decimal Long { get; set; }

        public CoffeeMakerTelemetry LastCoffeeMakerTelemetry { get; set; }
        public FootTrafficTelemetry LastFootTrafficTelemetry { get; set; }
        public FuelTankTelemetry LastFuelTankTelemetry { get; set; }

        public StoreIndexModel()
        {

        }

        public StoreIndexModel(DinocoStore store)
            : this()
        {
            if (store == null)
                return;

            this.StoreNumber = store.StoreID;
            this.AddressLine = $"{store.Address1} {store.City}, {store.State} {store.Zip}";
            this.ManagerLine = $"Manager: {store.StoreManager}";
            this.ContactLine = $"Phone: {store.StorePhone}";
            this.Lat = store.Lat;
            this.Long = store.Long;
        }
    }
}
