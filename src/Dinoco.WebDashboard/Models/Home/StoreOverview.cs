﻿using Dinoco.Shared.Models;
using Dinoco.Shared.Models.Telemetry;

namespace Dinoco.WebDashboard.Models.Home
{
    public class StoreOverview
    {
        public int StoreID { get; set; }

        public int CoffeeAge { get; set; }
        public double CoffeeTemperature { get; set; }


        public string CoffeeOverview
        {
            get
            {

                if (CoffeeAge <= 15)
                {
                    return "Fresh";
                }
                else if (CoffeeAge <= 40)
                {
                    return "Just Made";
                }
                else
                {
                    return "Old";
                }
            }
        }

        public int PeopleInStore { get; set; }

        public string FootTrafficOverview
        {
            get
            {

                if (PeopleInStore <= 10)
                {
                    return $"Slow - {PeopleInStore} souls";
                }
                else if (PeopleInStore <= 20)
                {
                    return $"Steady- {PeopleInStore} souls";
                }
                else
                {
                    return $"Busy- {PeopleInStore} souls";
                }
            }
        }

        public decimal E85Level { get; set; }
        public decimal AllinolLevel { get; set; }
        public decimal DieselLevel { get; set; }

        public string E85DisplayClass
        {
            get
            {
                if (E85Level <= 15)
                {
                    return "fuel-danger";
                }
                else if (E85Level <= 50)
                {
                    return "fuel-warning";
                }
                else
                {
                    return "fuel-good";
                }
            }
        }



        public string AllinolDisplayClass
        {
            get
            {
                if (AllinolLevel <= 15)
                {
                    return "fuel-danger";
                }
                else if (AllinolLevel <= 50)
                {
                    return "fuel-warning";
                }
                else
                {
                    return "fuel-good";
                }
            }
        }

        public string DieselDisplayClass
        {
            get
            {
                if (DieselLevel <= 15)
                {
                    return "fuel-danger";
                }
                else if (DieselLevel <= 50)
                {
                    return "fuel-warning";
                }
                else
                {
                    return "fuel-good";
                }
            }
        }


        public StoreOverview()
        {

        }

        public StoreOverview(DinocoStore store)
            : this()
        {
            if (store == null)
                return;

            this.StoreID = store.StoreID;
        }

        public void LoadFootTrafficResults(FootTrafficTelemetry dataAsObj)
        {
            if (dataAsObj == null)
                return;

            this.PeopleInStore = dataAsObj.Souls;
        }

        public void LoadFuelTankResults(FuelTankTelemetry dataAsObj)
        {
            if (dataAsObj == null)
                return;


            this.E85Level = dataAsObj.E85;
            this.AllinolLevel = dataAsObj.Allinol;
            this.DieselLevel = dataAsObj.Diesel;
        }

        public void LoadCoffeeMakerResults(CoffeeMakerTelemetry dataAsObj)
        {
            if (dataAsObj == null)
                return;

            this.CoffeeAge = dataAsObj.MinutesSinceLastBrew;
            this.CoffeeTemperature = dataAsObj.Temperature;
        }
    }
}
