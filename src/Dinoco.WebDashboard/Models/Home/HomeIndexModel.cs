﻿using System.Collections.Generic;

namespace Dinoco.WebDashboard.Models.Home
{
    public class HomeIndexModel
    {
        public List<StoreOverview> Stores { get; set; }

        public HomeIndexModel()
        {
            Stores = new List<StoreOverview>();
        }
    }
}
