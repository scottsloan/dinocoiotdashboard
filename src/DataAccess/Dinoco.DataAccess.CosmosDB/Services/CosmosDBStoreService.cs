﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dinoco.DataAccess.Interfaces;
using Dinoco.Shared.Models;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;

namespace Dinoco.DataAccess.CosmosDB.Services
{
    public class CosmosDBStoreService : IStoreService
    {
        private Container _container;

        public CosmosDBStoreService(IConfiguration config)
        {
            IConfigurationSection configurationSection = config.GetSection("CosmosDb");

            string databaseName = configurationSection.GetSection("DatabaseName").Value;
            string account = configurationSection.GetSection("Account").Value;
            string key = configurationSection.GetSection("Key").Value;
            CosmosClient client = new CosmosClient(account, key);

            this._container = client.GetContainer(databaseName, CosmosDBConstants.Stores_Container);
        }

        public async Task<List<DinocoStore>> GetAllStores()
        {

            var query = this._container.GetItemQueryIterator<DinocoStore>(new QueryDefinition("SELECT * FROM c"));
            List<DinocoStore> results = new List<DinocoStore>();
            while (query.HasMoreResults)
            {
                var response = await query.ReadNextAsync();

                results.AddRange(response.ToList());
            }

            return results;
        }

        public async Task<DinocoStore> GetStoreByStoreNumber(int StoreID)
        {
            var _allStores = await GetAllStores();
            return _allStores.Where(s => s.StoreID == StoreID).SingleOrDefault();
        }
    }
}
