﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dinoco.DataAccess.Interfaces;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;
using Dinoco.DataAccess.CosmosDB.Services.Models;


namespace Dinoco.DataAccess.CosmosDB.Services
{
    public class CosmosDBDeviceService : IDeviceService
    {
        private Container _container;

        public CosmosDBDeviceService(IConfiguration config)
        {
            IConfigurationSection configurationSection = config.GetSection("CosmosDb");

            string databaseName = configurationSection.GetSection("DatabaseName").Value;
            string account = configurationSection.GetSection("Account").Value;
            string key = configurationSection.GetSection("Key").Value;
            CosmosClient client = new CosmosClient(account, key);

            this._container = client.GetContainer(databaseName, CosmosDBConstants.IotHubMesssages_Container);
        }

        public async Task<List<string>> GetDevicesForStore(int StoreID)
        {
            List<string> values = new List<string>();

            QueryDefinition qd = new QueryDefinition("SELECT distinct c.DeviceType FROM c Where c.StoreID = @StoreID")
                                                .WithParameter("@StoreID", StoreID);

            using (FeedIterator<DeviceTypeResult> qA = _container.GetItemQueryIterator<DeviceTypeResult>(qd, requestOptions: new QueryRequestOptions { MaxConcurrency = 1 }))
            {
                while (qA.HasMoreResults)
                {
                    foreach (DeviceTypeResult val in await qA.ReadNextAsync())
                    {
                        if (values.Contains(val.DeviceType))
                            continue;

                        values.Add(val.DeviceType);
                    }
                }
            }

            return values;
        }

        public async Task<List<object>> GetLastReadingsForDevice(int StoreID, string DeviceType, int Take = 100)
        {
            List<object> results = new List<object>();
            string sql = "SELECT top @n * FROM c Where c.DeviceType = @DeviceType Order By c.Timestamp desc";

            if(StoreID > 0)
            {
                sql = "SELECT top @n * FROM c Where c.StoreID = @StoreID and c.DeviceType = @DeviceType Order By c.Timestamp desc";
            }

            QueryDefinition qd = new QueryDefinition(sql)
                                                .WithParameter("@n", Take)
                                                .WithParameter("@StoreID", StoreID)
                                                .WithParameter("@DeviceType", DeviceType);

            using (FeedIterator<object> qA = _container.GetItemQueryIterator<object>(qd, requestOptions: new QueryRequestOptions { MaxConcurrency = 1 }))
            {
                while (qA.HasMoreResults)
                {
                    foreach (object val in await qA.ReadNextAsync())
                    {
                        results.Add(val);
                    }
                }
            }

            return results;
        }
    }
}
