namespace Dinoco.DataAccess.CosmosDB.Services.Models
{
    public class DeviceTypeResult
    {
        public string DeviceType { get; set; }
    }
}