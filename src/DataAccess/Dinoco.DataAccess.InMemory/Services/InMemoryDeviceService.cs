﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dinoco.DataAccess.Interfaces;

namespace Dinoco.DataAccess.InMemory.Services
{
    public class InMemoryDeviceService : IDeviceService
    {
        public async Task<List<string>> GetDevicesForStore(int StoreID)
        {
            return new List<string> { "CoffeeMaker", "FuelTank", "FootTraffic" };
        }

        public async Task<List<object>> GetLastReadingsForDevice(int StoreID, string DeviceType, int Take = 100)
        {
            List<object> objs = new List<object>();

            for (int i = 0; i < Take; i++)
            {
                objs.Add(new
                {
                    StoreID = StoreID,
                    DeviceType = DeviceType,
                    TimeStamp = DateTime.Now
                });
            }

            return objs;
        }
    }
}
