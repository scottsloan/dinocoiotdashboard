﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dinoco.DataAccess.Interfaces;
using Dinoco.Shared.Models;

namespace Dinoco.DataAccess.InMemory.Services
{
    public class InMemoryStoreService : IStoreService
    {
        private List<DinocoStore> _stores { get; set; }

        public InMemoryStoreService()
        {
            _stores = new List<DinocoStore>();
            _stores.Add(new DinocoStore() { StoreID = 1, Address1 = "9515 WI-16 Trunk", City = "Onalaska", State = "WI", Zip = "54650", Lat = 43.87891562027628m, Long = -91.17763470663354m, StoreManager = "Mike Wazowski", StorePhone = "(555)555-5555" });
            _stores.Add(new DinocoStore() { StoreID = 2, Address1 = "3908 Circle Dr", City = "Holmen", State = "WI", Zip = "54636", Lat = 43.92541346539015m, Long = -91.24425476327421m, StoreManager = "T. Rex", StorePhone = "(555)555-5555" });
            _stores.Add(new DinocoStore() { StoreID = 3, Address1 = "4415 WI-16", City = "La Crosse", State = "WI", Zip = "54601", Lat = 43.87252432524103m, Long = -91.1934352239367m, StoreManager = "Carl Fredrickson", StorePhone = "(555)555-5555" });
        }

        public async Task<List<DinocoStore>> GetAllStores()
        {
            return _stores;
        }

        public async Task<DinocoStore> GetStoreByStoreNumber(int StoreID)
        {
            return _stores.Where(s => s.StoreID == StoreID).SingleOrDefault();
        }
    }
}
