﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dinoco.DataAccess.Interfaces
{
    public interface IDeviceService
    {
        Task<List<string>> GetDevicesForStore(int StoreID);
        Task<List<object>> GetLastReadingsForDevice(int StoreID, string DeviceType, int Take = 100);
    }
}
