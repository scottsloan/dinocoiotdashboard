﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dinoco.Shared.Models;

namespace Dinoco.DataAccess.Interfaces
{
    public interface IStoreService
    {
        Task<List<DinocoStore>> GetAllStores();
        Task<DinocoStore> GetStoreByStoreNumber(int StoreID);
    }
}
