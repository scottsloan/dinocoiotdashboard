﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dinoco.Devices;
using Dinoco.Shared.Enums.Devices;
using Dinoco.Shared.Models.Config;
using Dinoco.Shared.Models.Telemetry;
using Microsoft.Azure.Devices.Client;

namespace Dinoco.Devices
{
    public class CoffeeMakerDevice : IoTDeviceBase
    {
        private double _curTemp = 170.0d;
        private int _curMinutesSinceLastBrew = 0;
        private CoffeeMakerState _curState = CoffeeMakerState.BREWING;
        private bool _CompletedfirstBrew = false;

        public CoffeeMakerDevice(DeviceClient client, IoTDeviceConfig config)
            : base(client, config)
        {

        }

        public override async Task PerformOperationsAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                // When we first start up, we need to brew a fresh pot of coffee
                // so we won't generate new telemetry until we do  so
                if (_CompletedfirstBrew)
                {
                    await GenerateTelemery();
                }

                _CompletedfirstBrew = true;

                await SendTelemetryAsync();
                await Task.Delay(_config.OperationFreq * 1000 * 60);
            }
        }

        protected override async Task GenerateTelemery()
        {
            Random r = new Random();

            _curMinutesSinceLastBrew += 1;

            //If we the last state reported was different than Keep Warm
            //Assume that state is done and move on
            if (_curState != CoffeeMakerState.KEEPWARM)
            {
                _curState = CoffeeMakerState.KEEPWARM;
            }

            // Brew a new Pot.
            if (_curMinutesSinceLastBrew >= CoffeeMakerTelemetry.MaximumAge)
            {
                await base.Sendmessage(new GeneralMessageTelemetry("Alert", "Old Coffee!") { StoreID = base.StoreID });
                _curState = CoffeeMakerState.BREWING;
                _curMinutesSinceLastBrew = 0;
                _curTemp = CoffeeMakerTelemetry.MaximumTemp;
                await base.Sendmessage(new GeneralMessageTelemetry("INFO", "Brewing More Coffee") { StoreID = base.StoreID });

            } //reheat the exisitng pot
            else if (_curTemp <= CoffeeMakerTelemetry.MinimumTemp)
            {
                await base.Sendmessage(new GeneralMessageTelemetry("Alert", "Coffee Coffee!") { StoreID = base.StoreID });
                _curState = CoffeeMakerState.WARMING;
                _curTemp = CoffeeMakerTelemetry.MaximumTemp;
            }
            else
            {
                // Coffee is going cold.
                _curTemp -= CoffeeMakerTelemetry.HeatLost;
            }
        }

        protected override async Task SendTelemetryAsync()
        {
            CoffeeMakerTelemetry _payload = new CoffeeMakerTelemetry(_curState, _curTemp, _curMinutesSinceLastBrew);
            _payload.StoreID = this.StoreID;

            await base.Sendmessage(_payload);
        }
    }
}