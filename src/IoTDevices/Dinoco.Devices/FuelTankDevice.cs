﻿using System;
using System.Threading.Tasks;
using Dinoco.Shared.Models.Config;
using Dinoco.Shared.Models.Telemetry;
using Microsoft.Azure.Devices.Client;

namespace Dinoco.Devices
{
    public class FuelTankDevice : IoTDeviceBase
    {
        private decimal _e85Gallons;
        private decimal _allGallons;
        private decimal _dieselGallons;

        public FuelTankDevice(DeviceClient client, IoTDeviceConfig config)
            : base(client, config)
        {
            _e85Gallons = _allGallons = _dieselGallons = FuelTankTelemetry.MaxTankCapacity;
        }

        protected override async Task GenerateTelemery()
        {
            Random r = new Random();

            if (_e85Gallons <= 0)
            {
                await base.Sendmessage(new GeneralMessageTelemetry("INFO", "Refilling E85 Tank") { StoreID = base.StoreID });
                _e85Gallons = FuelTankTelemetry.MaxTankCapacity;
            }

            if (_allGallons <= 0)
            {
                await base.Sendmessage(new GeneralMessageTelemetry("INFO", "Refilling Allinol Tank") { StoreID = base.StoreID });
                _allGallons = FuelTankTelemetry.MaxTankCapacity;
            }

            if (_dieselGallons <= 0)
            {
                await base.Sendmessage(new GeneralMessageTelemetry("INFO", "Refilling Diesel Tank") { StoreID = base.StoreID });
                _dieselGallons = FuelTankTelemetry.MaxTankCapacity;
            }

            //Get a random number of cars that we served
            int rCarCount = r.Next(0, 5);

            for (int i = 0; i < rCarCount; i++)
            {
                //get random fuel consumption
                decimal consumption = (decimal)(r.NextDouble() * 10);

                //get random Fuel Type
                switch (r.Next(1, 4))
                {
                    case 1:
                        _e85Gallons -= (consumption > _e85Gallons) ?  _e85Gallons : (decimal)consumption;
                        break;
                    case 2:
                        _allGallons -= (consumption > _allGallons) ?  _allGallons : (decimal)consumption;
                        break;
                    case 3:
                        _dieselGallons -= (consumption > _dieselGallons) ?  _dieselGallons : (decimal)consumption;
                        break;
                }
            }

            if (_e85Gallons <= 0)
            {
                await base.Sendmessage(new GeneralMessageTelemetry("ALERT", "E85 Tank Empty") { StoreID = base.StoreID });
            }

            if (_allGallons <= 0)
            {
                await base.Sendmessage(new GeneralMessageTelemetry("ALERT", "Allinol Tank Empty") { StoreID = base.StoreID });
            }

            if (_dieselGallons <= 0)
            {
                await base.Sendmessage(new GeneralMessageTelemetry("ALERT", "Diesel Tank Empty") { StoreID = base.StoreID });
            }
        }

        protected override async Task SendTelemetryAsync()
        {
            FuelTankTelemetry _payload = new FuelTankTelemetry();
            _payload.StoreID = this.StoreID;
            _payload.E85 = (_e85Gallons / FuelTankTelemetry.MaxTankCapacity) * 100;
            _payload.Allinol = (_allGallons / FuelTankTelemetry.MaxTankCapacity) * 100;
            _payload.Diesel = (_dieselGallons / FuelTankTelemetry.MaxTankCapacity) * 100;

            await base.Sendmessage(_payload);
        }
    }
}
