﻿using System;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Dinoco.Devices.Interfaces;
using Dinoco.Shared.Models.Config;
using Microsoft.Azure.Devices.Client;

namespace Dinoco.Devices
{
    public class IoTDeviceBase : IIoTDevice
    {
        protected DeviceClient _devClient;
        protected IoTDeviceConfig _config;

        public int StoreID { get; set; }

        public IoTDeviceBase(DeviceClient client, IoTDeviceConfig config)
        {
            _devClient = client;
            _config = config;
        }

        public virtual async Task PerformOperationsAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await GenerateTelemery();
                await SendTelemetryAsync();
                await Task.Delay(_config.OperationFreq * 1000 * 60);
            }
        }

        protected virtual async Task GenerateTelemery()
        {

        }

        protected virtual async Task SendTelemetryAsync()
        {

        }

        protected async Task Sendmessage(object payload)
        {
            string telemetryPayload = JsonSerializer.Serialize(payload);

            Console.WriteLine($"[{DateTime.Now}] Sending Telemetry {telemetryPayload}");

            using var message = new Message(Encoding.UTF8.GetBytes(telemetryPayload))
            {
                ContentEncoding = "utf-8",
                ContentType = "application/json",
            };

            await _devClient.SendEventAsync(message);
        }
    }
}
