﻿using System;
using System.Threading.Tasks;
using Dinoco.Shared.Models.Config;
using Dinoco.Shared.Models.Telemetry;
using Microsoft.Azure.Devices.Client;

namespace Dinoco.Devices
{
    public class FootTrafficDevice : IoTDeviceBase
    {
        private int _souls;

        public FootTrafficDevice(DeviceClient client, IoTDeviceConfig config)
            : base(client, config)
        {

        }

        protected override async Task GenerateTelemery()
        {
            Random r = new Random();

            int MaxDelta = _souls < 3 ? 10 : _souls;
            int rInt = r.Next(0, MaxDelta);

            // Create a 60% that people are leaving
            // Have found otherwise that the stores tend to fill up
            if (r.Next(0, 100) > 40)
            {
                rInt *= -1;
            }

            _souls += rInt;

            if (_souls < 0)
                _souls = 0;
        }

        protected override async Task SendTelemetryAsync()
        {
            FootTrafficTelemetry _payload = new FootTrafficTelemetry(_souls);
            _payload.StoreID = this.StoreID;

            await base.Sendmessage(_payload);
        }
    }
}
