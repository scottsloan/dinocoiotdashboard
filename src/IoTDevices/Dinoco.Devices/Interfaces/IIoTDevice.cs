﻿using System.Threading;
using System.Threading.Tasks;

namespace Dinoco.Devices.Interfaces
{
    public interface IIoTDevice
    {
        int StoreID { get; set; }
        Task PerformOperationsAsync(CancellationToken cancellationToken);
    }
}
