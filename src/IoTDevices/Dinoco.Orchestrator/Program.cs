﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Dinoco.Devices.Interfaces;
using Dinoco.Shared.Models.Config;
using Microsoft.Extensions.Configuration;

namespace Dinoco.Orchestrator
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                           .SetBasePath(Directory.GetCurrentDirectory())
                           .AddJsonFile("appSettings.json", optional: false);

            IConfiguration config = builder.Build();

            Console.WriteLine("Welcome to the Orchestrator!");
            Console.WriteLine("Press Control+C to quit the sample.");

            using var ctk = new CancellationTokenSource(Timeout.InfiniteTimeSpan);
            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                eventArgs.Cancel = true;
                ctk.Cancel();
                Console.WriteLine("Shutting Down...");
            };

            List<IoTDeviceConfig> devConfigs = config.GetSection("IoTDevices").Get<List<IoTDeviceConfig>>();

            List<Task> _devices = new List<Task>();
            foreach (var devConfig in devConfigs)
            {
                IIoTDevice _dev = await DeviceFactory.ConfigureDevice(devConfig, ctk.Token);

                if (_dev != null)
                {
                    var t = _dev.PerformOperationsAsync(ctk.Token);
                    _devices.Add(t);
                }
            }

            await Task.WhenAll(_devices);
        }
    }
}
