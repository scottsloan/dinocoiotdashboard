﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dinoco.Devices;
using Dinoco.Devices.Interfaces;
using Dinoco.Shared.Models.Config;
using Microsoft.Azure.Devices.Client;

namespace Dinoco.Orchestrator
{
    public static class DeviceFactory
    {
        public static async Task<IIoTDevice> ConfigureDevice(IoTDeviceConfig config, CancellationToken cancellationToken)
        {
            IIoTDevice _sensor = null;

            var devClient = await SetupDeviceClientAsync(config.Registration, cancellationToken);

            switch (config.DeviceType.ToLower())
            {
                case "foottraffic":
                    _sensor = new FootTrafficDevice(devClient, config);
                    break;

                case "fueltank":
                    _sensor = new FuelTankDevice(devClient, config);
                    break;

                case "coffeemaker":
                    _sensor = new CoffeeMakerDevice(devClient, config);
                    break;
            }

            _sensor.StoreID = config.StoreID;
            return _sensor;
        }


        private static async Task<DeviceClient> SetupDeviceClientAsync(RegistrationInfo registration, CancellationToken cancellationToken)
        {
            DeviceClient deviceClient = null;

            switch (registration.RegistrationType.ToLower())
            {
                case "connectionstring":
                    deviceClient = DeviceClient.CreateFromConnectionString(registration.ConnectionString, TransportType.Mqtt, null);
                    break;

                default:
                    throw new ArgumentException($"RegistrationType is not set for sensor");
            }

            return deviceClient;
        }
    }
}
